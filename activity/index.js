/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printCustomerInfo() {
		let fullName = prompt ("Please enter your full name")
		let age = prompt ("How old are you?")
		let location = prompt("Where are you from?")

		console.log("Hello, " + fullName)
		console.log("You are " + age + " years old.")
		console.log("You live in " + location)
}
printCustomerInfo()


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printBands() {
		alert("Please enter your 5 most favorite band!")
		let firstBand = prompt ("1st most favorite band:")
		let secondBand = prompt ("2nd most favorite band:")
		let thirdBand = prompt ("3rd most favorite band:")
		let fourthBand = prompt ("4th most favorite band:")
		let fifthBand = prompt ("5th most favorite band:")
		console.log("Here are my 5 most favorite bands of all time:")
		console.log("1. " + firstBand)
		console.log("2. " + secondBand)
		console.log("3. " + thirdBand)
		console.log("4. " + fourthBand)
		console.log("5. " + fifthBand)
	}

printBands()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovies(){
		console.log ("5 Most Favorite Movies:")
		console.log("1. LOTR 1")
		console.log("Rotten Tomatoes Rating: 98%")
		console.log("2. LOTR 2")
		console.log("Rotten Tomatoes Rating: 97%")
		console.log("3. LOTR 3")
		console.log("Rotten Tomatoes Rating: 98%")
		console.log("4. The Hobbit 1")
		console.log("Rotten Tomatoes Rating: 96%")
		console.log("5. The Hobbit 2")
		console.log("Rotten Tomatoes Rating: 95%")
	}

favoriteMovies()
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends()