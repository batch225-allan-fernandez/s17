// Basic Functions

// [SECTION] Function Declaration

/*
	The statements and instructions inside a function is not immediately executed when the function is defined.
	They are run/executed when a function is invoked.
	To invoke a declared function, add the name of the function and a parenthesis.

*/
  // Hoisting - javascript behavior for certain variables and functions to run to use them before their declaration

function printName(){
	console.log("My Name is John");
	console.log("I love cats")
	console.log("Nana was my favorite");
}

printName();

// Function Expression

let variableFunction = function myGreetings() {
	console.log("Hello");
}

variableFunction();

// Re-Assigning Functions

variableFunction = function myGreetings () {
	console.log("Updated Hello");
}

variableFunction()
// ================================================
// Function Scoping
/*
Scope is the accessibility(visibility) of variables
a. local/block scope
b. global scope
c. function scope
*/
// ================================================

// Local Variable - inside the curly braces
{
	let localVar = "Armando Perez";
	console.log(localVar)
} 
// ================================================
// Global Variable
let globalVar = "Mr. Worldwide";

console.log(globalVar);

{
	console.log(globalVar)
}
// ================================================
// Function Scope

function showNames() {
	// Function Scoped Variables:
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst)
	console.log(functionLet)
}

showNames();

// ================================================
// Alert()

// alert() - allows us to show a small window at the top of our browser page to show information to our users. It, much like alert(), will have the page wait until the user completes or enters their input.

alert("Hello, User!");

function showSampleAlert() {
	alert("Hello, User!")
}

showSampleAlert()

// ================================================

// Promt ()

let samplePrompt = prompt("Enter your Name: ")

console.log("Hello, " + samplePrompt)

function printWelcomeMessages() {
	let firstName = prompt ("Enter Your First Name")
	let lastName = prompt("Enter Your Last Name")

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to Mobile Legends")
}

printWelcomeMessages()



